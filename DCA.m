function  [result] = DCA(Netlist,varargin)
% DCA DCA applies a BLA-based Distortion Contribution Analysis to an ADS netlist
% 
%      result = DCA(Netlist);
%      result = DCA(Netlist,MSdef);
%      result = DCA(Netlist,MSdef,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)|iscellstr(x)
%      ADS netlist of the circuit that's analysed. You can provide
%      the filename  of the netlist or the cell array of strings that
%      contains the statements
% 
% Optional inputs:
% - MSdef     default: []  check: @isstruct
%      Describes the multisine which is used as an excitation signal.
%      Use  MScreate to generate this struct. It also needs an MSnode
%      field to  indicate where the multisine should be connected to.
% 
% Parameter/Value pairs:
% - 'portnr'     default: 2  check: @isscalar
%      port number where the DCA should be performed
% - 'Z0'     default: 50  check: @isscalar
%      characteristic impedance used to generate the waves
% - 'numberOfRealisations'     default: 10  check: @isscalar
%      number of multisine phase realisations used to calculate the
%      covariance  matrix of the distortion sources
% - 'display'     default: false  check: @islogical
%      Indicates whether the results should be displayed
% - 'oversample'     default: 10  check: @isscalar
%      estimated order of non-linearity of the circuit
% - 'BLAtechnique'     default: 'Sparam'  check: @(x) any(strcmpi(x,{'Sparam','ZIPPER'}))
%      indicates whether the BLA should be used or not. If this is
%      false, the  small-signal S-parameters of the subblocks are used.
%      Otherwise, the Best  Linear Approximation is calculated using
%      an LSSS analysis. set useBLA  only to true if you really need
%      it, because is requires a very large  simulation time
% - 'CF'     default: Inf  check: @isscalar
%      maximum crest factor of the multisines used in the simulations
% - 'outOfBandFactor'     default: 2  check: @isscalar
%      if useBLA is set to true, you can control the out of band factor
%      with  this number. If it is set to 1, the BLA is only estimated
%      in-band
% - 'ticklerDistance'     default: 1e-6  check: @isscalar
%      the LSSS analysis uses a tickler which lies off the multisine
%      grid to  estimate the BLA. This distance in Hz can be specified
%      here
% - 'showSteps'     default: false  check: @islogical
%      indicates whether intermediate results should be shown. This
%      generates a  whole lot of plots, so don't be surprised
% - 'loadOldResults'     default: false  check: @islogical
%      indicates whether old simulation results should be loaded.
% - 'saveSimResults'     default: false  check: @islogical
%      use this to save the simulation results
% - 'cleanup'     default: true  check: @islogical
%      indicates whether temporary simulation files are deleted afterwards
%      or  not. usefull for debugging purposes
% - 'testPhase'     default: []  check: @(x) any(strcmpi(x,{'BLA','Comp'}))
%      if you want the DCA to be performed up to a certain level, you
%      can use  this testPhase parameter. If you give this one of the
%      possible values,  the function stops at the specified point
%      and just returns the results up  to that point
% - 'freq'     default: []  check: @isnumeric
%      frequency at which the DCA results should be plotted
% - 'unbal2bal'     default: true  check: @islogical
%      can be used to control the conversion from unbalanced to balanced
%       circuits in case there are differential ports in the circuit
% - 'uncorr'     default: false  check: @islogical
%      indicates that the BLAs of different stages are uncorrelated
% 
% Outputs:
% - result      Type: struct
%      the result struct contains a whole lot of information that has
%      been generated during the DCA.
% 
% The ports of the different Stages are indicated with current probes with a special
% name Naming convention for the current probes is "I_Stagename_pX", where Stagename
% is the name of the stage and X is the number of the port. The current probes point
% into the stage. If the port is a mixed port (encountered in differential systems),
% you can add 'p' and 'm' to the name of the current probe to indicate the plus and
% the minus port respectively. the names of the current probes then look like "I_Stagename_pXp"
% and "I_Stagename_pXm" with the same port number X The ports of the total system
% are indicated with I_TOTAL_pX where X is the number of the port. The port parameter
% in the DCA statement refers to the  number of the TOTAL port in the circuit.
% 
% Adam Cooman, ELEC, VUB
% 
% 
% This documentation was generated with the generateFunctionHelp function at 21-Apr-2016



p=inputParser();
p.FunctionName = 'DCA';

% ADS netlist of the circuit that's analysed. You can provide the filename
% of the netlist or the cell array of strings that contains the statements
p.addRequired('Netlist',@(x) ischar(x)|iscellstr(x));

% Describes the multisine which is used as an excitation signal. Use
% MScreate to generate this struct. It also needs an MSnode field to
% indicate where the multisine should be connected to.
p.addOptional('MSdef',[],@isstruct);

% port number where the DCA should be performed
p.addParameter('portnr',2,@isscalar);

% characteristic impedance used to generate the waves
p.addParamValue('Z0',50,@isscalar);

% number of multisine phase realisations used to calculate the covariance
% matrix of the distortion sources
p.addParamValue('numberOfRealisations',10,@isscalar);

% Indicates whether the results should be displayed
p.addParamValue('display',false,@islogical);

% estimated order of non-linearity of the circuit
p.addParamValue('oversample',10,@isscalar);

% indicates whether the BLA should be used or not. If this is false, the
% small-signal S-parameters of the subblocks are used. Otherwise, the Best
% Linear Approximation is calculated using an LSSS analysis. set useBLA
% only to true if you really need it, because is requires a very large
% simulation time
p.addParamValue('BLAtechnique','Sparam',@(x) any(strcmpi(x,{'Sparam','ZIPPER'})));

% maximum crest factor of the multisines used in the simulations
p.addParamValue('CF',Inf,@isscalar);

% if useBLA is set to true, you can control the out of band factor with
% this number. If it is set to 1, the BLA is only estimated in-band
p.addParamValue('outOfBandFactor',2,@isscalar);

% the LSSS analysis uses a tickler which lies off the multisine grid to
% estimate the BLA. This distance in Hz can be specified here
p.addParamValue('ticklerDistance',1e-6,@isscalar);

% indicates whether intermediate results should be shown. This generates a
% whole lot of plots, so don't be surprised
p.addParamValue('showSteps',false,@islogical);

% indicates whether old simulation results should be loaded.
p.addParamValue('loadOldResults',false,@islogical);

% use this to save the simulation results
p.addParamValue('saveSimResults',false,@islogical);

% indicates whether temporary simulation files are deleted afterwards or
% not. usefull for debugging purposes
p.addParamValue('cleanup',true,@islogical);

% if you want the DCA to be performed up to a certain level, you can use
% this testPhase parameter. If you give this one of the possible values,
% the function stops at the specified point and just returns the results up
% to that point
p.addParamValue('testPhase',[],@(x) any(strcmpi(x,{'BLA','Comp'})));

% frequency at which the DCA results should be plotted
p.addParamValue('freq',[],@isnumeric);

% can be used to control the conversion from unbalanced to balanced
% circuits in case there are differential ports in the circuit
p.addParamValue('unbal2bal',true,@islogical);

% indicates that the BLAs of different stages are uncorrelated
p.addParamValue('uncorr',false,@islogical);
p.parse(Netlist,varargin{:});
DCAinfo = p.Results;
clear p varargin

% open the netlist file and get the statements in a cell array
if ~iscell(DCAinfo.Netlist); Netlist = readTextFile(DCAinfo.Netlist); else Netlist = DCAinfo.Netlist; DCAinfo = rmfield(DCAinfo,'Netlist'); end

% find the multisine statement(s) in the netlist
if isempty(DCAinfo.MSdef); [MSdef,Netlist] = ExtractMSinfo(Netlist); else MSdef = DCAinfo.MSdef; end

if ~iscell(MSdef.MSnode)
    MSdef.MSnode = {MSdef.MSnode '0'};
end

% detect the stages in the system
[StageInfo.Extern,StageInfo.Stages,StageInfo.StageNames,~,~,StageInfo.diffExPorts,StageInfo.diffStPorts]...
    = analyseNetlist(Netlist);

% get the DCA statement in the netlist and extract its info
if (nargin>1)
    % if custom values are provided to the function, the DCAinfo in the netlist is ignored
    [~,Netlist] = ExtractDCAinfo(Netlist,StageInfo.diffExPorts);
else
    % if no custom values are provided to the function, the DCAinfo is extracted from the netlist
    [DCAinfo,Netlist] = ExtractDCAinfo(Netlist,StageInfo.diffExPorts);
end

% if people want to see the settings of the method, show them
if DCAinfo.showSteps; showDCAsettings(DCAinfo,MSdef,StageInfo); end

% determine the BLA of all the sub-blocks in the network
try
    if DCAinfo.loadOldResults; load(fullfile(cd,'BLAs.mat')); else error('to the catch!'); end
catch
    [BLAs,waves,spec,wavesraw,specraw] = DCA_estimate_BLAs(Netlist,MSdef,'technique',DCAinfo.BLAtechnique,...
        'cleanup',DCAinfo.cleanup,'unbal2bal',DCAinfo.unbal2bal,'CF',DCAinfo.CF,...
        'numberOfRealisations',DCAinfo.numberOfRealisations);
    if DCAinfo.saveSimResults; save('BLAs.mat','BLAs','waves','spec'); end
end

% if we are in testing mode, just return the BLAs without doing anything else
if strcmpi(DCAinfo.testPhase,'BLA'); result = BLAs; return; end

% if needed, calculate some more phase realisations of the multisine
try
    if DCAinfo.loadOldResults; load(fullfile(cd,'MultisineSimulations.mat')); else error('to the catch!'); end
catch
    if ~isempty(spec)
        AlreadySimulated = size(spec.(MSdef.MSnode{1}),1);
    else
        AlreadySimulated = 0;
    end
    if AlreadySimulated>=DCAinfo.numberOfRealisations
        DCAinfo.numberOfRealisations = AlreadySimulated;
        wavesnew = [];specnew=[];
    else
        % simulate the wave response to the multisine
        [wavesnew,specnew] = ADSsimulateMSwaves(Netlist,MSdef,'simulator','HB','oversample',DCAinfo.oversample,'numberOfRealisations',DCAinfo.numberOfRealisations-AlreadySimulated,'Z0',DCAinfo.Z0,'CF',DCAinfo.CF);
    end
    % combine the old and the new simulations
    spec = combineSimstructs(spec,specnew);
    waves = combineSimstructs(waves,wavesnew);
    clear specnew wavesnew
    % save the simulation results
    if DCAinfo.saveSimResults; save('MultisineSimulations.mat','waves','spec'); end
end

% combine the mixed waves into differential and common-mode waves
if DCAinfo.unbal2bal; waves = process_Waves(waves); end

% add the reference signal to the waves struct
waves.ref = spec.(MSdef.MSnode{1});

% calculate the covariance matrix of all distortion sources
[BLAs,Cd] = Calculate_Distortion_v4(waves,BLAs,'S',DCAinfo.uncorr);

% if the function should only be run up to the compensation step, exit it here
if strcmpi(DCAinfo.testPhase,'Comp');result.BLAs = BLAs;result.waves = waves; return;end

% calculate the contributions of each of the stages to each of the output waves
[CONTs,BLAs] = ContributionAnalysis(BLAs,Cd);

% calculate the correlation matrix between the distortion at the output waves 
% when we consider the system as a SIMO system from reference to output waves
[BLAs.TERMINATED.Cd_SIMO,~] = SIMO_BLA_check(waves,'ref',DCAinfo.unbal2bal);

% calculate the predicted SIMO BLAs to be able to compare them to the
% obtained SIMO BLA from reference to the different waves in the circuit
SIMO = predict_SIMO_BLA(BLAs);


% put the final results in a struct
result.DCAinfo = DCAinfo;
result.MSdef = MSdef;
result.BLAs = BLAs;
result.waves = waves;
result.spec = spec;
result.wavesraw = wavesraw;
result.specraw = specraw;
result.CONTs = CONTs;
result.Cd = Cd;
result.SIMO = SIMO;



%% Plotting of all the results
if DCAinfo.display
    % plot the different estimated BLAs
    plotBLAs(BLAs);
    % plot the waves at the ports of the total circuit, the SIMO prediction
    % of the distortion and the contribution of the TOTAL circuit to those waves
    P = length(BLAs.TOTAL.porttype);
    for pp=1:P
        % plot the B-wave at the total port
        fig=figure(200+pp);
        set(fig,'Name',sprintf('W_TOTAL_p%s_B',BLAs.TOTAL.porttype{pp}));
        plot(waves.freq,db(squeeze(waves.(sprintf('W_TOTAL_p%s_B',BLAs.TOTAL.porttype{pp}))(:,1,:))),'k.');
        hold on
        plot(BLAs.freq,db(squeeze(BLAs.TERMINATED.Cd_SIMO(pp,pp,:)))/2,'ro');
        plot(BLAs.freq,db(squeeze(sum(sum(CONTs.(['TOTAL_p' num2str(pp)]),1),2)))/2,'b*');
        title(sprintf('W_TOTAL_p%s_B',BLAs.TOTAL.porttype{pp}),'Interpreter','none');
        legend('measured waves','sum of contributions','SIMO bla result')
    end
    % plot the sorted contributions, the actual result of the DCA
    plotContributions(CONTs,'freqToPlot',[],'MStype',MSdef.info);
end

end



    

%%
function showDCAsettings(DCAinfo,MSdef,StageInfo)
% this small function shows the info provided to the DCA function
    disp('The extracted MSdef is:'); 
    disp(MSdef); 
    disp('The extracted DCAinfo is:'); 
    disp(DCAinfo);
    disp('The extracted Stages are:');
    disp(StageInfo);
end


%%
function plotBLAs(BLAs)
% this function plots the information that's found in the BLAs struct

% first extract the subcircuits
subcircuits = fieldnames(BLAs);
subcircuits = subcircuits(cellfun(@(x) ~any(strcmp(x,{'PACKAGE','LOAD','TERMINATED','TOTAL','freq','freq_lin','Z'})),subcircuits));
% S is the amount of subcircuits
S = length(subcircuits);
% plot the BLA of all the substages
ind = 1;
for ss=1:S
    switch S
        case 1
            subplot(1,1,ind);
        case 2
            subplot(1,2,ind);
        otherwise
            subplot(ceil(sqrt(S-1)),ceil(sqrt(S-1)),ind);
    end
    plot_mimo(BLAs.freq,db(BLAs.(subcircuits{ss}).G));
    ind = ind+1;
    title(subcircuits{ss})
end

% plot the TOTAL system
if isfield(BLAs,'TOTAL')
    figure('name','TOTAL circuit')
    leg = plot_mimo(BLAs.freq,db(BLAs.TOTAL.G),'r-');
    if isfield(BLAs.TOTAL,'G_conn')
        hold on
        plot_mimo(BLAs.freq,db(BLAs.TOTAL.G_conn),'b+');
        plot_mimo(BLAs.freq,db(BLAs.TOTAL.G_conn-BLAs.TOTAL.G),'ko');
    end
    legend(leg);
end

end

% @generateFunctionHelp
% @tagline DCA applies a BLA-based Distortion Contribution Analysis to an ADS netlist
% @author Adam Cooman
% @institution ELEC, VUB

% @output1 the result struct contains a whole lot of information that has
% @output1 been generated during the DCA.
 
% @outputType1 struct

% @extra The ports of the different Stages are indicated with current probes with a special name
% @extra Naming convention for the current probes is "I_Stagename_pX", where
% @extra Stagename is the name of the stage and X is the number of the port.
% @extra The current probes point into the stage.
% @extra If the port is a mixed port (encountered in differential systems), you
% @extra can add 'p' and 'm' to the name of the current probe to indicate the plus
% @extra and the minus port respectively. the names of the current probes then look
% @extra like "I_Stagename_pXp" and "I_Stagename_pXm" with the same port number X
% @extra The ports of the total system are indicated with I_TOTAL_pX where X is
% @extra the number of the port. The port parameter in the DCA statement refers to the 
% @extra number of the TOTAL port in the circuit.


