clear all
close all
clc

cd('V:\Toolbox\DistortionContributionAnalysis\Demo\Folded Cascode Opamp');
delete('BLAs.mat');

MSdef = MScreate(0.1e6,10e6,'grid','odd','amp',0.0005);
MSdef.MSnode = {'MS_node','sgnd'};

RESbal = DCA('FoldedCascOpamp_incomb.net','MSdef',MSdef,'useBLA',false,'numberOfRealisations',50,'unbal2bal',true,...
    'loadOldResults',true,'save',true,'cleanup',true,'showsteps',true,'display',false);
delete('BLAs.mat');
RESunbal = DCA('FoldedCascOpamp_incomb.net','MSdef',MSdef,'useBLA',false,'numberOfRealisations',50,'unbal2bal',false,...
    'loadOldResults',true,'save',false,'cleanup',true,'showsteps',true,'display',false);


%% This following piece of code generates the figures for the SMACD paper
close all
figloc = 'C:\users\adam.cooman\Google Drive\Contributies\SMACD 2015\figs\';
export = true;
bin = 51;

h = plotContributions(RESunbal.CONTs,bin*MSdef.f0,RESunbal.MSdef.info,10);
close(h(1));
% ylim([0 10]);
title('')
ylabel('[%]')
set(h(2),'Position',[100,400,500,150],'color','w')
if export;export_fig(fullfile(figloc,'FOLDED_RESunbal'),'-pdf','-nocrop');end

h = plotContributions(RESbal.CONTs,bin*MSdef.f0,RESbal.MSdef.info,10);
close(h(1));
% ylim([0 25]);
title('')
ylabel('[%]')
set(h(2),'Position',[100,100,500,150],'color','w')
if export;export_fig(fullfile(figloc,'FOLDED_RESbal'),'-pdf','-nocrop');end

h = figure('name','error made in the balanced analysis');
plot(RESbal.waves.freq(1:300)/1e6,db(squeeze(RESbal.waves.W_TOTAL_p2_B(:,1,1:300)).'),'k.')
hold on
plot(RESbal.BLAs.freq/1e6,db(squeeze(RESbal.BLAs.TERMINATED.Cd_test(4,4,:)))/2,'bo');
plot(RESbal.BLAs.freq/1e6,db(squeeze(RESbal.BLAs.TERMINATED.Cd_conn(4,4,:)))/2,'r*');
ylim([-250 -60])
ylabel('Output wave')
xlabel('frequency [MHz]')
set(h,'Position',[100,700,500,200],'color','w')
if export;export_fig(fullfile(figloc,'FOLDED_error'),'-pdf','-nocrop');end