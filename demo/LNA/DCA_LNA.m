close all
clear all
clc
cd V:\Toolbox\DistortionContributionAnalysis\Demo\LNA

res = DCA('LNA_v4.net','useBLA',false,'numberOfRealisations',50,'bin',11,...
    'loadOldResults',false,'save',false,'cleanup',true,'showsteps',true,'display',false);
%%

% plot the contributions at one of the frequencies 
plotContributions( res.CONTs , 11e6 , res.MSdef.info );
% plot the contributions at all the frequencies
plotContributions( res.CONTs , []   , res.MSdef.info );