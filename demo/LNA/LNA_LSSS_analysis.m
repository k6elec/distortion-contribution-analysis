% this script runs the LSSS_analysis on the LNA example
cd V:\Toolbox\DistortionContributionAnalysis\demo\LNA\

clear all
clc
close all

% open the netlist file and get the statements in a cell array
Netlist = readTextFile('LNA_LSSS.net');

% find the multisine statement(s) in the netlist
[MSdef,Netlist] = ExtractMSinfo(Netlist);

% detect the stages in the system
[StageInfo.Extern,StageInfo.Stages,StageInfo.StageNames,~,~,StageInfo.diffExPorts,StageInfo.diffStPorts] = analyseNetlist(Netlist);

% get the DCA statement in the netlist and extract its info
[DCAinfo,Netlist] = ExtractDCAinfo(Netlist,StageInfo.diffExPorts);

DCAinfo.numberOfRealisations = 3;

% run the BLA_analysis to determine the BLA of the stages
[Sp , Sd , Sd_lin , Sd_var , bigwaves] = LSSS_Analysis( Netlist , MSdef , ...
    'oversample' , DCAinfo.oversample , 'numberOfRealisations' , DCAinfo.numberOfRealisations , ...
    'outOfBandFactor' , 2 , 'ticklerStep', 4 , 'ticklerDistance' , 1e3 , 'showResults' , true );

