clc
clear all

% The main info for the DCA is given in the netlist itself.
% There is a multisine source
%   MS:MSsource ref 0 fmin=10e6 fmax=1000e6 gridtype="random-odd" amp=0.001
% which defines the multisine to be used.
% and there is a DCA statement
%   DCA:DCA1 port="2" numberOfRealisations=3 display="res"
% which contains the port at which the distortion contribution analysis
% will be performed, the number of realisations of the multisine and the
% display type

DCA_v3('LNA_v4.net',11);