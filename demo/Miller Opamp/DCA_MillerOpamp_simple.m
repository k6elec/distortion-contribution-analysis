close all
clear all
clc

addpath(genpath('V:\Toolbox\'));

cd V:\Toolbox\DistortionContributionAnalysis\Demo\Miller' Opamp'\
delete('BLAs.mat');

MSdef = MScreate(1e5,10e6,'grid','odd','amp',0.001);
MSdef.MSnode = {'MS_node','sgnd'};

% RESbal = DCA('MillerOpamp_zoom.net','MSdef',MSdef,'BLAtechnique','Sparam','numberOfRealisations',50,'unbal2bal',true,...
%     'loadOldResults',true,'save',false,'cleanup',true,'showsteps',true,'display',false);

RES = DCA('MillerOpamp_simple.net','MSdef',MSdef,'BLAtechnique','Sparam','numberOfRealisations',20,'unbal2bal',false,...
    'loadOldResults',false,'save',true,'cleanup',true,'showsteps',false,'display',false);
%%
plotContributions(RES.CONTs);

%% This following piece of code generates the figures for the SMACD paper
close all
figloc = '';
export = false;

h = plotContributions(RES.CONTs,51*MSdef.f0,RES.MSdef.info);
close(h(1));
% ylim([0 15]);
title('')
ylabel('[%]')
set(h(2),'Position',[100,400,500,150],'color','w')
if export;export_fig(fullfile(figloc,'MILLER_res_simple'),'-pdf','-nocrop');end

% h = figure('name','error made in the balanced analysis');
% plot(RES.waves.freq(1:300)/1e6,db(squeeze(RES.waves.W_TOTAL_p2_B(:,1,1:300)).'),'k.')
% hold on
% plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.TERMINATED.Cd_test(4,4,:)))/2,'bo');
% plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.TERMINATED.Cd_conn(4,4,:)))/2,'r*');
% ylim([-250 -60])
% ylabel('Output wave')
% xlabel('frequency [MHz]')
% set(h,'Position',[100,700,500,200],'color','w')
% if export;export_fig(fullfile(figloc,'MILLER_error'),'-pdf','-nocrop');end

%%
figure(101)
stem3(squeeze(RES.CONTs.TOTAL_p2(:,:,50)))

%% bonus pictures for the presentation
close all


h = figure('name','error made in the balanced analysis');
plot(RES.waves.freq(1:300)/1e6,db(squeeze(RES.waves.W_TOTAL_p2_B(:,1,1:300)).'),'k.')
ylim([-250 -60])
ylabel('Output wave')
xlabel('frequency [MHz]')
set(h,'Position',[700,700,500,200],'color','w')

if export;export_fig('OUTPUT_raw','-png','-nocrop','-m5');end


h = figure('name','error made in the balanced analysis');
[signal,time]=ADSconvert2timeDomain(RES.spec,'fieldfilter',{'MS_node'});
subplot(2,3,1)
[c,b]=hist(squeeze(vec(signal.MS_node(1:10,end,:))),50);
barh(b,c/max(c));
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);
ylabel('Voltage [V]')
subplot(2,3,[2 3])
plot(time*1e6,squeeze(signal.MS_node(1,end,:)),'k')
xlabel('time [us]')

subplot(2,3,[4 5 6]);
plot(RES.spec.freq(1:300)/1e6,db(squeeze(RES.spec.MS_node(:,1,1:300)).'),'k.')
ylim([-600 10])
ylabel('Multisine voltage [dBV]')
xlabel('frequency [MHz]')
set(h,'Position',[700,100,500,400],'color','w')

if export;export_fig('INPUT_raw','-png','-nocrop','-m5');end
