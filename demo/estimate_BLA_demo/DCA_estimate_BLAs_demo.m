% this demo shows how to use the DCA_estimate_BLAs function to get the Best
% Linear Approximation of each of the sub-circuits in a circuit

clear all
close all
clc

% build the multisine
MSdef = MScreate(1e6,100e6,'rms',0.001,'grid','random-odd');
MSdef.MSnode = {'ref','0'};

% the netlist contains an LNA
Netlist = 'DCA_estimate_BLAs_demo_LNA.net';

% call DCA_estimate BLAs
[BLAs,waves,spec] = DCA_estimate_BLAs(Netlist,MSdef,'technique','SUBSTRACT','numberOfRealisations',2);

%%
% plot all the stuff
close all
systems = fieldnames(BLAs);
for ss=1:length(systems)
    if ~any(strcmp(systems{ss},{'freq','freq_lin','PACKAGE','LOAD'}))
        figure('name',systems{ss})
        [varTest,bins] = testVariance(BLAs.(systems{ss}).G,15);
        plot_mimo(BLAs.freq,db(BLAs.(systems{ss}).G));
        plot_mimo(BLAs.freq,db(BLAs.(systems{ss}).varG)/2);
        plot_mimo(BLAs.freq(bins),db(varTest)/2);
        plot_mimo(BLAs.freq_lin,db(BLAs.(systems{ss}).G_linear),'+');
    end
end

figure('name','waves at the ports of the total system')
subplot(221)
plot(waves.freq,db(squeeze(waves.W_TOTAL_p1_A(:,1,:))).','+');
title('p1_A')
subplot(222)
plot(waves.freq,db(squeeze(waves.W_TOTAL_p2_A(:,1,:))).','+');
title('p2_A')
subplot(223)
plot(waves.freq,db(squeeze(waves.W_TOTAL_p1_B(:,1,:))).','+');
title('p1_B')
subplot(224)
plot(waves.freq,db(squeeze(waves.W_TOTAL_p2_B(:,1,:))).','+');
title('p2_B')

%% check the Z estimates of all the stages
for ss=1:length(systems)
    if ~any(strcmp(systems{ss},{'freq','freq_lin','PACKAGE','LOAD'}))
        Zinfo = BLAs.(systems{ss}).Zinfo;
        figure('name',systems{ss})
        for rr=1:length(Zinfo)
            N = 5;
            varTest = zeros(size(Zinfo(rr).Z,1),size(Zinfo(rr).Z,2)-N);
            for ii=1:size(Zinfo(rr).Z,1);
                [varTest(ii,:),bins] = testVariance(Zinfo(rr).Z(ii,:),N);
            end
            plot(Zinfo(rr).freq,db(Zinfo(rr).Z));
            hold on
            plot(Zinfo(rr).freq,db(Zinfo(rr).varZ)/2,'+');
            plot(Zinfo(rr).freq(bins),db(varTest)/2,'o');
        end
    end
end

