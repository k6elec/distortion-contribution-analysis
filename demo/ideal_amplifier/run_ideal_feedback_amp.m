% I test whether the distortion at the input and output of a non-linear
% system in feedback can be predicted using the BLA of that system.


MSdef = MScreate(1e5,1e7,'rms',0.2,'grid','random-odd');
MSdef.MSnode = 'ref';

spec = ADSsimulateMS('ideal_amp_netlist.net',MSdef,'simulator','HB','numberofrealisations',120);

%%
close all

% show the results of the simulation
plot(spec.freq,squeeze(db(spec.out)),'+');


% calculate the BLA
[B,Y,U] = calculateSISO_BLA(spec,'input','in','output','out','reference','ref');

figure(2)
subplot(121)
plot(B.freq,db(B.mean));
hold on;
plot(B.freq,db(B.stdNL));
title('BLA and its standard deviation')
subplot(122)
plot(B.freq_interp,db(B.Ys),'+');
title('distortion source Ys at even and odd bins')

% calculate the actuator and feedback dynamics
omega = 2*pi*B.freq_interp;
R1 = 5e3*ones(size(omega));
R2 = 10e3*ones(size(omega));
C2 = 10e-12*ones(size(omega));
Z2 = 1./(1./R2 + 1i.*omega.*C2);

% G are the generator dynamics
G = 1-R1./(R1+Z2);
% H are the feedback dynamics
H = -R1./(R1+Z2);

% refer the distortion to the input and the output:
Ysu = B.Ys .* H./(1+B.mean_interp.*H);
Ysy = B.Ys .* 1./(1+B.mean_interp.*H);


% plot the input spectrum, the mean input spectrum and the distortion at the input spectrum
figure(4);
subplot(121)
plot(spec.freq(1:round(end/4)),db(squeeze(spec.in(:,:,1:round(end/4)))),'b.');
hold on;
plot(B.freq,db(U.mean),'k+');
plot(spec.freq(1:round(end/4)),db(U.disto(1:round(end/4))),'g+');
plot(B.freq_interp,db(Ysu),'r+');
title('input spectrum');

% plot the output spectrum, the mean output spectrum and the distortion at the output spectrum
subplot(122)
plot(spec.freq(1:round(end/4)),db(squeeze(spec.out(:,:,1:round(end/4)))),'b.');
hold on;
plot(B.freq,db(Y.mean),'k+');
plot(spec.freq(1:round(end/4)),db(Y.disto(1:round(end/4))),'g+');
plot(B.freq_interp,db(Ysy),'r+');
title('output spectrum');



