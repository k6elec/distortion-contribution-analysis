% I test whether the distortion at the input and output of a non-linear
% system in feedback can be predicted using the BLA of that system.
clear all
close all
clc


MSdef = MScreate(1e4,5e6,'rms',0.01,'grid','random-odd');
MSdef.MSnode = 'ref';

spec = ADSsimulateMS('ideal_amp_2stage_netlist.net',MSdef,'simulator','HB','numberofrealisations',12);

% show the results of the simulation
plot(spec.freq,squeeze(db(spec.out)),'+');

%%

% calculate the BLA of the first stage
[BLA,OUT,IN,~,CovYs] = calculateSISO_BLA(spec,'input',{'amp','inte'},'output',{'inte','out'},'reference','ref');

 
figure(2)
subplot(221)
plot(BLA(1).freq,db(BLA(1).mean),'k+-');
hold on;
plot(BLA(1).freq,db(BLA(1).stdNL));
[varTest,ax] = testVariance(BLA(1).mean,3);
plot(BLA(1).freq(ax),db(squeeze(varTest))/2,'m');
title('BLA and its standard deviation')
subplot(222)
plot(BLA(1).freq_interp(1:2:end),db(BLA(1).Ys(1:2:end)),'r+');
hold on
plot(BLA(1).freq_interp(2:2:end),db(BLA(1).Ys(2:2:end)),'b+');
title('distortion source Ys at even and odd bins')
% second stage
subplot(223)
plot(BLA(2).freq,db(BLA(2).mean),'k+-');
hold on;
plot(BLA(2).freq,db(BLA(2).stdNL));
[varTest,ax] = testVariance(BLA(2).mean,3);
plot(BLA(2).freq(ax),db(squeeze(varTest))/2,'m');
title('BLA and its standard deviation')
subplot(224)
plot(BLA(2).freq_interp(1:2:end),db(BLA(2).Ys(1:2:end)),'r+');
hold on
plot(BLA(2).freq_interp(2:2:end),db(BLA(2).Ys(2:2:end)),'b+');
title('distortion source Ys at even and odd bins')

%%

% calculate the actuator and feedback dynamics
omega = 2*pi*B1.freq_interp;
R1 = 5e3*ones(size(omega));
R2 = 10e3*ones(size(omega));
C2 = 10e-12*ones(size(omega));
Z2 = 1./(1./R2 + 1i.*omega.*C2);
% G are the generator dynamics
G = 1-R1./(R1+Z2);
% H are the feedback dynamics
H = -R1./(R1+Z2);

denom = (1+BLA(1).mean_interp.*BLA(2).mean_interp.*H);

% refer the distortion to the input and the output:
Ys1_out = BLA(1).Ys .* BLA(2).mean_interp./denom;
Ys2_out = BLA(2).Ys .*              1./denom;
Ys_out_sum = abs(Ys1_out) + abs(Ys2_out);

Ys1_int = BLA(1).Ys .* 1                  ./denom;
Ys2_int = BLA(2).Ys .* H .* BLA(1).mean_interp./denom;
Ys_int_sum = abs(Ys1_int) + abs(Ys2_int);

Ys1_in = BLA(1).Ys .* H .* BLA(2).mean_interp./denom;
Ys2_in = BLA(2).Ys .* H                  ./denom;
Ys_in_sum = abs(Ys1_in) + abs(Ys2_in);

% plot the input spectrum, the mean input spectrum and the distortion at the input spectrum
figure(4);
subplot(131)
% plot(spec.freq(1:round(end/4)),db(squeeze(spec.amp(:,:,1:round(end/4)))),'b.');
hold on;
plot(BLA(1).freq,db(IN(1).mean),'k+');
plot(spec.freq(1:round(end/4)),db(IN(1).disto(1:round(end/4))),'g+');
plot(BLA(1).freq_interp,db(Ys_in_sum),'ro');
plot(BLA(1).freq_interp,db(Ys1_in),'y.');
plot(BLA(1).freq_interp,db(Ys2_in),'m.');
legend('exBins','disto','sum of disto','disto stage 1','disto stage 2');
title('input spectrum');

subplot(132)
% plot(spec.freq(1:round(end/4)),db(squeeze(spec.amp(:,:,1:round(end/4)))),'b.');
hold on;
plot(BLA(1).freq,db(OUT(1).mean),'k+');
plot(spec.freq(1:round(end/4)),db(OUT(1).disto(1:round(end/4))),'g+');
plot(BLA(1).freq_interp,db(Ys_int_sum),'ro');
plot(BLA(1).freq_interp,db(Ys1_int),'y.');
plot(BLA(1).freq_interp,db(Ys2_int),'m.');
title('iternal node spectrum');
legend('exBins','disto','sum of disto','disto stage 1','disto stage 2');

% plot the output spectrum, the mean output spectrum and the distortion at the output spectrum
subplot(133)
% plot(spec.freq(1:round(end/4)),db(squeeze(spec.out(:,:,1:round(end/4)))),'b.');
hold on;
plot(BLA(2).freq,db(OUT(2).mean),'k+');
plot(spec.freq(1:round(end/4)),db(OUT(2).disto(1:round(end/4))),'g+');
plot(BLA(2).freq_interp,db(Ys_out_sum),'ro');
plot(BLA(2).freq_interp,db(Ys1_out),'y.');
plot(BLA(2).freq_interp,db(Ys2_out),'m.');
title('output spectrum');
legend('exBins','disto','sum of disto','disto stage 1','disto stage 2');



