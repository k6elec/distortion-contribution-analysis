function [CONTs,BLAs] = ContributionAnalysis(BLAs,Cd)
% calculate the separate contributions if the connection scattering matrix and the distortion covariance matrix are known
%
%   [Contributions,BLAs] = ContributionAnalysis(BLAs,Cd)
%
%   BLAs is the struct that contains the different BLAs in the circuit
%   Cd is the PxPxF covariance matrix of the distortion sources in the circuit
%
% Adam Cooman, ELEC VUB

% calculate the connection scattering matrix of the system
% Winv contains F square matrices of size (P_TOTAL + P_TOTAL + sum(P_STAGES) + sum(P_STAGES)) 
%   the first P_TOTAL ports contain the transfer from the internal sources to the ports of the TOTAL circuit
%   then there are P_TOTAL + sum(P_STAGES) ports for the package
%   finally, there are sum(P_STAGES) ports for the sub-circuits
Winv = Calculate_Winv(BLAs);

% with this Winv matrix, we can caculate the covariance matrix of the A-waves in the circuit
%   Ca = Winv * Cd * Winv'
% where Ca has the following ordering of waves
% | X X             |   A-waves at the ports of the TOTAL circuit
% | X X             |
% |     X X X X     |
% |     X X X X     |   A-waves at the package matrix
% |     X X X X     |   
% |     X X X X     |
% |             X X |   A-waves at the stages stage
% |             X X |
%
% We assume that the package contains no contributions, so the only
% non-zero part in the covariance matrix is the last bit of size(sum(P_STAGES));
% so Cd has the following shape
% | 0 0             |   A-waves at the ports of the TOTAL circuit
% | 0 0             |
% |     0 0 0 0     |
% |     0 0 0 0     |   A-waves at the package matrix
% |     0 0 0 0     |   
% |     0 0 0 0     |
% |             X X |   A-waves at the stages stage
% |             X X |
%
% We are only interested in the A-waves at the ports of the TOTAL circuit,
% so we don't have to calculate the whole A-matrix. Only the first P_TOTAL
% ports of it actually.
% We can calculate the contibution at only one port by picking a row out of
% the Winv matrix:
%   Ca(p) = Winv(p)* Cd * Winv(p)'
% Cd is zeros, everywhere except at the end, where the S-matrices of the
% stages are located, so we can use only the last sum(P_STAGES) part of the
% Winv(p) vector.
%
% Because we want the contributions of everything separately, we rewrite
% the Ca(p) expression using kron to obtain the product of a column and a
% row vector:
%   Ca(p) = (conj(Winv(p)) kron Winv(p)) * vec(Cd) = sum( (conj(Winv(p)) kron Winv(p)) .* vec(Cd) )
% which tells us that each element in Cd is transported to the output by
% the corresponding term in the kron product.
%
% For convenience, I calculate all the elements of the sum and put those
% back in the ContributionMatrix
% 
% The upper and lower triangle elements of the contributionmatrix are
% complex conjugate.
% In the final step, I combine them back together into an upper diagonal
% matrix that has only the real parts of the contributions added to them

% P is the total amount of contributing ports in the analysis
% F is the amount of frequencies used in the analysis
[P_STAGES,~,F] = size(Cd);
P_TOTAL = size(BLAs.TOTAL.G,1);

% first calculate the output-referred distortion matrix Ca
BLAs.TOTAL.Cd_conn = zeros(P_TOTAL,P_TOTAL,F);
for ff=1:F
    % calculate the output referred distortion
    BLAs.TOTAL.Cd_conn(:,:,ff) = Winv(1:P_TOTAL,end-P_STAGES+1:end,ff) * Cd(:,:,ff) * (Winv(1:P_TOTAL,end-P_STAGES+1:end,ff)');
end

% gather the different frequency responses from distortion source to the output
BLAs.Tout = zeros(P_TOTAL,P_STAGES,F);
for pp=1:P_TOTAL
    BLAs.Tout(pp,:,:) = Winv(pp,end-P_STAGES+1:end,:);
end


% now calculate the contributions to each port with the kronecker sum
for pp=1:P_TOTAL
    
    % preallocate the ContMat
    ContMat = zeros(size(Cd));
    for ff=1:F
        Wimportant =  Winv(pp,end-P_STAGES+1:end,ff);
        Contributions = (kron(conj(Wimportant),Wimportant).*(vec(Cd(:,:,ff)).')).';
        ContMat(:,:,ff) = reshape(Contributions,[P_STAGES P_STAGES]);
    end
    % combine the complex conjugate parts and save them on only one side of
    % the ContMat. The other half is just zero
    for ii=1:P_STAGES
        for jj=1:P_STAGES
            if ii==jj
                ContMat(ii,jj,:) = ContMat(ii,jj,:);
            elseif ii>jj
                ContMat(ii,jj,:) = 2*real(ContMat(ii,jj,:));
            else
                ContMat(ii,jj,:) = 0;
            end
        end
    end
    % save this contribution matrix in the CONTs struct
    CONTs.(sprintf('TOTAL_p%s',BLAs.TOTAL.porttype{pp})) = ContMat;
end

% add the names of the ports as an extra field to the CONTs struct. This is
% to keep track of the name of the contribution
CONTs.PortNames = BLAs.PACKAGE.portNames(P_TOTAL+1:end);

% add the frequency axis as well
CONTs.freq = BLAs.freq;

end