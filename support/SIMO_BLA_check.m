function [Cd,freq] = SIMO_BLA_check(waves,refNodeName,unbal2bal)
% this function takes the measured B waves at the ports of the total
% system and considers them as the ouputs of a SIMO system. with the
% reference multisine as the input.
%
%   Cd = SIMO_BLA_check(waves, exBins,Extern,refNodeName)
%
% Inputs
%   waves           The struct that contains the Mx1xF simulation results.
%                    This function looks only at the fields W_TOTAL_p%d_B and freq
%   refNodeName     name of the node where the multisine is connected to,
%                   this is used to correct the phase of the waves before averaging
%
% Outputs
%   Cd              [Extern x Extern x F] correlation matrix of the
%                   non-linear distortion at the ports of the TOTAL system
%   
%                  -----------
%   --\/\/\-------|           |----------
%   |        B1<~~|           |<~~A2    |
%  MS        A1~~>|           |~~>B2    Rl
%   --------------|           |----------
%                  -----------
%
%
%         ----------   
%        |          |  |Ys1
%        |   SIMO   |--o----- B1
%        |    BLA   | 
%  MS ---|          |  |Ys2 
%        |          |--o----- B2
%        |          |
%         ---------- 
%
% Adam Cooman, ELEC VUB

if unbal2bal
    p = 'dc';
else
    p = 'pm';
end

% P is the amount of ports in the total circuit
P = sum(cellfun(@(x) ~isempty(x),regexp(fieldnames(waves),sprintf('W_TOTAL_p[0-9]+%s?_[AB]',p(1)))))/2;

% build the vector with ports of the TOTAL circuit
% Afields = {};
Bfields = {};
for pp=1:P
    if isfield(waves,sprintf('W_TOTAL_p%d%s_A',pp,p(1)))
%         Afields{end+1} = sprintf('W_TOTAL_p%d%s_A',pp,p(1));
%         Afields{end+1} = sprintf('W_TOTAL_p%d%s_A',pp,p(2));
        Bfields{end+1} = sprintf('W_TOTAL_p%d%s_B',pp,p(1));
        Bfields{end+1} = sprintf('W_TOTAL_p%d%s_B',pp,p(2));
    else
%         Afields{end+1} = sprintf('W_TOTAL_p%d_A',pp);
        Bfields{end+1} = sprintf('W_TOTAL_p%d_B',pp);
    end
end
% A-waves first, then the B-waves
fields = [Bfields].';

% calculate the SISO BLAs from reference to each output and calculate the
% distortion covariance matrix of the distortion sources in the circuit
[G, ~, ~, ~ , Cd ] = calculateSISO_BLA(waves,...
                  'reference',refNodeName,...
                  'input',repmat({refNodeName},length(fields),1),...
                  'output',fields);

freq = G(1).freq;


end


%
%         ----------   |Ys1
%        |          |--o----- A1
%        |          |
%        |          |  |Ys2
%        |          |--o----- A2
%  MS ---|   SIMO   |
%        |    BLA   |  |Ys3
%        |          |--o----- B1
%        |          |
%        |          |  |Ys4 
%        |          |--o----- B2
%         ---------- 
%