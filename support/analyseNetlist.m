function [Extern,Stages,StageNames,ExternNodes,StageNodes,diffExPorts,diffStPorts] = analyseNetlist(NetlistFile)
% Analyses a netlist to be used in the distortion contribution analysis
%
%	[Extern,Stages,StageNames] = analyseNetlist(NetlistFile)
%
% This function analyses the netlist file and looks for current probes with
% a name of the form "I_name_px" or their differential counterpart "I_name_px[p|m]" 
% where name is the name of the sub-circuit and x is an integer indicating the port number. 
% is the name is TOTAL, the port will be considered as a port of the total
% system.
%
% Extern        Scalar indicating how many ports the total system contains.
% StageNames    cell array which contains the names of the detected stages.
% Stages        Vector with the same length as StageNames, that contains
%               the amount of ports each stage has
% ExternNodes   cell array with the nodes of the external current probes
% StageNodes    cell array with cell arrays that contain the nodes the
%               current probes are connected to.
% diffExPorts   array of port numbers that indicate the differential ports of the total system.
% diffStPorts   cell array of port number vectors that indicate the differential ports of each stage.
%
% Adam Cooman, ELEC VUB
% 18/10/2013
%
% TODO: this function can be made a whole lot faster if the regexp call is
% not done in a for loop, but only once and on the full netlist immediately 

p = inputParser();
p.addRequired('NetlistFile',@(x)ischar(x)||iscell(x));
p.parse(NetlistFile);
args = p.Results;

% If the netlist is a string, just read in the file
if ~iscell(args.NetlistFile)
    data = readTextFile(args.NetlistFile);
else
    data = args.NetlistFile;
end

% Extern is a scalar which indicates how many external ports are present.
Extern=0;
% Stages is a vector which length indicates the amount of stages and each index contains the amount of ports on the specific stage.
Stages=[];
% ExternNodes is a cell array with the nodes where the external current sources are connected to
ExternNodes={};
% StageNames is a cell array with the names of the different stages
StageNames = {};
% StageNodes is a cell array with the nodes the current sources are connected to
StageNodes = {};
% an array with the ports numbers of the differential ports on the total system
diffExPorts = [];
% cell array of arrays with ports that are differential on each stage
diffStPorts = {};

% totalstring is the string used to indicate the ports of the total system
totalstring = 'TOTAL';

%  we look for "Short: I_name_py" or "Short: I_name_py[p|m]"
findstr = '^\s*Short\s*\:\s*I_(?<stagename>[0-9a-zA-Z]+)_p(?<portnr>[1-9][0-9]*)(?<diff>[pm]?)\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+';

for ii=1:length(data)
    % check for the short
    t = regexp(data{ii},findstr,'names');
    if ~isempty(t)
        if strcmp(t.stagename,totalstring)
            Extern=Extern+1;
            ExternNodes{str2double(t.portnr)} = {t.node1 t.node2};
            if strcmp(t.diff,'p')
                diffExPorts(end+1) = str2double(t.portnr);
            end
        else
            % look for the stagename in the
            ind = find(not(cellfun('isempty',strfind(StageNames,t.stagename))),1);
            if isempty(ind)
                StageNames{end+1} = t.stagename;
                Stages(end+1)=1;
                diffStPorts{end+1}=[];
                ind = length(Stages);
                if strcmp(t.diff,'p')
                    diffStPorts{ind}=str2double(t.portnr);
                end
            else
                Stages(ind) = Stages(ind)+1;
                if strcmp(t.diff,'p')
                    diffStPorts{ind}(end+1)=str2double(t.portnr);
                end
            end
        end
    end
end

% sort all the differential port numbers
diffExPorts = sort(diffExPorts);
for ii=1:length(diffStPorts)
    diffStPorts{ii}=sort(diffStPorts{ii});
end

% look for the nodes of the different current sources
for ss=1:length(Stages)
    StageNodes{ss}={};
    for pp=1:Stages(ss)-length(diffStPorts{ss})
        % if the port is differential, we should look for a p and m short
        if any(diffStPorts{ss}==pp)
            strp = ['^\s*Short\s*\:\s*I_' StageNames{ss} '_p' num2str(pp) 'p\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
            fnd = regexp(data,strp,'names');
            t = fnd{cellfun(@(x) ~isempty(x),fnd)};
            StageNodes{ss}{end+1} = {t.node1 t.node2};
            strm = ['^\s*Short\s*\:\s*I_' StageNames{ss} '_p' num2str(pp) 'm\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
            fnd = regexp(data,strm,'names');
            inds = cellfun(@(x) ~isempty(x),fnd);
            if ~any(inds)
                error(sprintf('encountered a problem while looking for %s_p%d',StageNames{ss},pp));
            else
                t = fnd{inds};
            end
            StageNodes{ss}{end+1} = {t.node1 t.node2};
        else
            strn = ['^\s*Short\s*\:\s*I_' StageNames{ss} '_p' num2str(pp) '\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
            fnd = regexp(data,strn,'names');
            inds = cellfun(@(x) ~isempty(x),fnd);
            if ~any(inds)
                error(sprintf('encountered a problem while looking for %s_p%d',StageNames{ss},pp));
            else
                t = fnd{inds};
            end
            StageNodes{ss}{end+1} = {t.node1 t.node2};
        end
    end
end

% also look for the extern nodes
ExternNodes={};
for pp=1:Extern-length(diffExPorts)
    % if the port is differential, we should look for a p and m short
    if any(diffExPorts==pp)
        strp = ['^\s*Short\s*\:\s*I_TOTAL_p' num2str(pp) 'p\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
        fnd = regexp(data,strp,'names');
        t = fnd{cellfun(@(x) ~isempty(x),fnd)};
        ExternNodes{end+1} = {t.node1 t.node2};
        strm = ['^\s*Short\s*\:\s*I_TOTAL_p' num2str(pp) 'm\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
        fnd = regexp(data,strm,'names');
        t = fnd{cellfun(@(x) ~isempty(x),fnd)};
        ExternNodes{end+1} = {t.node1 t.node2};
    else
        strn = ['^\s*Short\s*\:\s*I_TOTAL_p' num2str(pp) '\s+(?<node1>[a-zA-Z][a-zA-Z0-9_]*)\s+(?<node2>[a-zA-Z][a-zA-Z0-9_]*)\s+'];
        fnd = regexp(data,strn,'names');
        t = fnd{cellfun(@(x) ~isempty(x),fnd)};
        ExternNodes{end+1} = {t.node1 t.node2};
    end
end
            
            
end