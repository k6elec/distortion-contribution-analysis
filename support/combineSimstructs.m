function res = combineSimstructs(varargin)
% this function combines different simulation results in one structure.
%
%   res = combineSimstructs(s1,s2,...,sN)
%
% The result is a single struct where all fields of size [MxPxF] are
% combined is a single big [MxPxF] field, but now with a very big M
%
% Adam Cooman, ELEC VUB



% remove empty fields from the input
varargin = varargin(cellfun(@(x) ~isempty(x),varargin));
% check whether all inputs are structs
if ~all(cellfun(@isstruct,varargin))
    error('all inputs should be structs');
end
% chech whether all structs have the same fields
fields = cellfun(@fieldnames,varargin,'UniformOutput',false);
for ii=2:length(fields)
    try
        if ~all(strcmp(sort(fields{1}),sort(fields{ii})))
            error('to the catch!');
        end
    catch
        error('all structs should have the same fields.')
    end
end

% now combine the structs
res = varargin{1};
for ss=2:length(varargin)
    for ff=1:length(fields{ss})
        siz = size(varargin{ss}.(fields{ss}{ff}));
        if length(siz)==3
            res.(fields{ss}{ff})(end+1:end+siz(1),:,:)= varargin{ss}.(fields{ss}{ff});
        end
    end
end


end
