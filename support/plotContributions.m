function handles =  plotContributions(CONTs,varargin)
% plotContributions shows the output-referred contributions of a distortion contribution analysis
%
%   plotContributions(CONTs,freqToPlot,MStype,numContsToPlot)
%
% CONTs       struct that contains the info about the contributions. It has
%             the following fields:
%               - TOTAL_pX fields of size PxPxF, which give the
%               contributions of the sub-circuits to the specified node
%               - freq 1xF frequency vector of the contributuons
%               - PortNames cell array of length P that contains the names
%               of the ports in the 
% freqToPlot  Frequency at which to plot the contributions. If this is left
%             empty, a 3d plot is made with all the contributions
% MStype      info about the multisine type. If the multisine is an odd
%             multisine, the contributions are plotted on different figures 
%             for the even and the odd frequency bins
% numContsToPlot  is the amount of contributions to plot. default is 10
% combine     boolean that indicates whether or not the contributions of a
%             stage should be combined. Default = false
%
%
% Adam Cooman. ELEC VUB


p = inputParser();

% structure that contains the different distortion contributions in the
% circuit. This struct should be generated with the DCA function
p.addRequired('CONTs',@isstruct);

% type of the multisine. If an odd MS is provided, a separate plot will be
% generated for the even and odd frequency bins in the simulation.
p.addParameter('MStype','full',@(x) any(strcmpi(x,{'full','odd','random-odd'})));

% if this boolean is set to true, the different contributions per stage are
% summed to reduce the amount of total contributions.
p.addParameter('combine',true,@islogical);

% number of contributions to plot. If the amount of contributions is lower
% than this number, all of the contributions will be plotted.
p.addParameter('numContsToPlot',10,@isscalar);

% frequency at which the contributions should be plotted. The closest
% frequency bin to the provided frequency is detected. If this is left
% empty, all contributions will be plotted on a 3D plot.
p.addParameter('freqToPlot',[],@(x) isscalar(x)||isempty(x));

% number of the first figure that will be plotted. The function generates
% two plots, the first will be figure(Fignumber), the second will be
% figure(Fignumber+1)
p.addParameter('FigNumber',1,@isscalar);

% if this is set to true, no figure() call is made before plotting. This
% can be usefull to plot the contributions in subplots
p.addParameter('noFigure',0,@islogical);

% vector of port numbers at which to plot the distortion contributions. If
% this is not set, the contributions at all output ports will be plotted
p.addParameter('PortNumsToPlot',[],@isvector);

p.parse(CONTs,varargin{:});
args = p.Results;
clear CONTs varargin p

% if ~exist('MStype','var');MStype='full';end;
% if ~exist('freqToPlot','var');freqToPlot=[];end;
% if ~exist('combine','var');combine=false;end;

if args.combine
    % find out the unique amount of stages
    [newCONTs.PortNames,ia,ic] = unique(cellfun(@(x) strjoin(x(1:end-1)) ,regexp(args.CONTs.PortNames,'_','split'),'uniformoutput',false),'stable');
    P = length(newCONTs.PortNames);
    newCONTs.freq = args.CONTs.freq;
    F = length(newCONTs.freq);
    
    % determine the amount of ports in each stage
    numPorts = zeros(1,P);
    for ii=1:P-1
        numPorts(ii) = ia(ii+1)-ia(ii);
    end
    numPorts(P) = length(ic)-ia(end)+1;
    
    % combine the different contributions of each of the stages.
    fields = fieldnames(args.CONTs);
    fields = fields(cellfun(@(x) ~isempty(x),regexp(fields,'^TOTAL_p')));
    for ss=1:length(fields)
        newCONTs.(fields{ss}) = zeros(P,P,F);
        for ii=1:P
            for jj=1:P
                rng1=sum(numPorts(1:ii-1))+1:sum(numPorts(1:ii));
                rng2=sum(numPorts(1:jj-1))+1:sum(numPorts(1:jj));
                newCONTs.(fields{ss})(ii,jj,:)=sum(sum(args.CONTs.(fields{ss})(rng1,rng2,:),1),2);
            end
        end
    end
    
    % replace the original contribution struct by the new one and just use
    % the function as before
    args.CONTs = newCONTs;
end

% P is the amount of ports in the sub-circuits, so we will have P*P contributions
P = length(args.CONTs.PortNames);
% F is the amount of frequencies
F = length(args.CONTs.freq);

% check the amount of contributions to plot
if ((1+P)*P/2)<args.numContsToPlot
    args.numContsToPlot=(1+P)*P/2;
end

% build a matrix with the name of each contribution
NameCell = cell(P);
for ii=1:P
    for jj=1:P
        if ii==jj
            NameCell{ii,jj} = args.CONTs.PortNames{ii};
        else
            NameCell{ii,jj} = sprintf('%s * %s',args.CONTs.PortNames{ii},args.CONTs.PortNames{jj});
        end
    end
end
NameVec = vec(NameCell);

% put the contributions in a sorted vector again
fields = fieldnames(args.CONTs);
fields = fields(cellfun(@(x) ~isempty(x),regexp(fields,'^TOTAL_p')));
for pp=1:length(fields)
    sorted = zeros(P^2,F);
    order = zeros(P^2,F);
    for ff=1:F
        vect = vec(args.CONTs.(fields{pp})(:,:,ff));
        [~,order(:,ff)] = sort(abs(vect),'descend');
        sorted(:,ff) = vect(order(:,ff));
    end
    % the matrices are upper diagonal, so the amount of nonzero elements is given by sum(1:P)
    numConts = sum(1:P);
    args.CONTs.([fields{pp} '_vec'  ]) = sorted(1:numConts,:);
    args.CONTs.([fields{pp} '_order']) =  order(1:numConts,:);
end

% select the wanted ports
if ~isempty(args.PortNumsToPlot)
    % remove the numbers that are possibly too high and round to integers
    args.PortNumsToPlot = round(args.PortNumsToPlot(args.PortNumsToPlot<=length(fields)));
    % keep only the fields that are wanted
    fields = fields(args.PortNumsToPlot);
end

%% A frequency is passed, plot only the contributions at that specific frequency
if ~isempty(args.freqToPlot)
    [~,bin] = min(abs(args.CONTs.freq-args.freqToPlot));
    for pp=1:length(fields)
        if ~args.noFigure
            handles(pp)=figure(args.FigNumber+pp-1);
            set(handles(pp),'name',sprintf('Contributions to %s at %sHz',fields{pp},eng(args.CONTs.freq(bin))));
        end
        plotContributionVector(NameVec(args.CONTs.([fields{pp} '_order'])(:,bin)),args.CONTs.([fields{pp} '_vec'])(:,bin),[],args.numContsToPlot);
    end
else
    for pp=1:length(fields)
        if ~args.noFigure
            handles(pp) = figure(args.FigNumber+pp-1);
            set(handles(pp),'name',sprintf('Contributions to %s',fields{pp}));
        end
        NameMat = cell(size(args.CONTs.([fields{pp} '_vec'])));
        for ff=1:length(args.CONTs.freq)
            NameMat(:,ff) = NameVec(args.CONTs.([fields{pp} '_order'])(:,ff));
        end
        if any(strcmpi(args.MStype,{'random-odd','odd'}))
            subplot(2,1,1)
            plotContributionMatrix(NameMat(:,1:2:end),args.CONTs.([fields{pp} '_vec'])(:,1:2:end),args.CONTs.freq(1:2:end),args.numContsToPlot);
            title('1:2:end')
            subplot(2,1,2)
            plotContributionMatrix(NameMat(:,2:2:end),args.CONTs.([fields{pp} '_vec'])(:,2:2:end),args.CONTs.freq(2:2:end),args.numContsToPlot);
            title('2:2:end')
        else
            plotContributionMatrix(NameMat,args.CONTs.([fields{pp} '_vec']),args.CONTs.freq,args.numContsToPlot);
        end
    end    
end


end

%% function to plot a vector of contributions
function plotContributionVector(Names,Contributions,Total,numContsToPlot)
% Contributions is a vector of a whole lot of contributions, there should
% be a global that indicates the amount of stuff to plot

sumCont = sum(Contributions);
% put the contributions in a percentage
Contributions = Contributions*100/sumCont;

switch nargin
    case {1,2}
        if length(Contributions)>10
            numContsToPlot = 10;
        else
            numContsToPlot = length(Contributions);
        end
        % also scale the Total distortion in the same way
        Total = [];
    case 3
        if length(Contributions)>10
            numContsToPlot = 10;
        else
            numContsToPlot = length(Contributions);
        end
        Total =  Total*100/sumCont;
end

% C is the total amount of contributions
C = length(Contributions);

% find the elements on the diagonal of the contributionmatrix. These are
% the ones that have a name that doesn't contain a '*' to indicate the
% correlation
diagsELEMS = cellfun(@isempty,regexp(Names,'*'));

% plot the diagonal elements in blue
stem(diagsELEMS(1:numContsToPlot).*Contributions(1:numContsToPlot),...
    'LineWidth',6,'Marker','none','Color','b');
hold on
% plot the off diagonal elements in orange
stem((~diagsELEMS(1:numContsToPlot)).*Contributions(1:numContsToPlot),...
    'LineWidth',6,'Marker','none','Color',[1 0.4 0]);

% plot the 100% line, which is the sum of the contributions
% plot([0.5 numContsToPlot+0.5],[100 100],'r-','LineWidth',3);
if ~isempty(Total)
    plot([0.5 numContsToPlot+0.5],100*[1 1]*Total,'k--','LineWidth',2);
end
clear Ticks

for ii=1:numContsToPlot
    if any(Names{ii}=='*')
        color = [1 100/255 0];
    else
        color = [0 0 1];
    end
    if Contributions(ii)>0
        vertPos = Contributions(ii);
    else
        vertPos = 0;
    end
    text(ii,vertPos,Names{ii},'VerticalAlignment','baseline','HorizontalAlignment','center','Interpreter','none','Color',color);
end
% set(gca,'XTickLabel',Names(1:numContsToPlot));
xlim([0.5 numContsToPlot+0.5]);
set(gca,'XTickLabel',{});

ylabel('Distortion Contribution []');
grid on

end

%% function to plot a matrix of contributions
function plotContributionMatrix(Names,Contributions,FreqRange,numContsToPlot)
% Contributions is an MxF matrix with sorted contributions
% total is a 1xF vector that contains the total distortion of the circuitat that specific frequency
% freq is the frequency axis of the whole thing

if ~exist('numContsToPlot','var')
    if size(Contributions,1)>10
        numContsToPlot = 10;
    else
        numContsToPlot = size(Contributions,1);
    end
end

F = size(Contributions,2);
if length(FreqRange)~=F
    error('freq ax should have same size as total')
end
total = sum(Contributions,1);

% shift the total distortion up such that the minimum is zero, this way, the stem will look good.
min_disto = min(db(total));
dbshifted_total = db(total) - min_disto;
for ff=1:F
    Contributions(:,ff) = dbshifted_total(ff) * Contributions(:,ff)/total(ff);
end

diags = zeros(size(Contributions));
for ff=1:F
    diags(:,ff) = cellfun(@isempty,regexp(Names(:,ff),'*'));
end

% plot the contributions in blue and orange
for ff=1:length(FreqRange)
    stem3(FreqRange(ff)*ones(numContsToPlot,1) , 1:numContsToPlot , diags(1:numContsToPlot,ff).*Contributions(1:numContsToPlot,ff),'LineWidth',3,'Marker','none','Color','b');
    hold on
    % plot the off diagonal elements in orange
    stem3(FreqRange(ff)*ones(numContsToPlot,1) , 1:numContsToPlot , (~diags(1:numContsToPlot,ff)).*Contributions(1:numContsToPlot,ff),'LineWidth',3,'Marker','none','Color',[1 0.4 0]);
end

% plot the total distortion at the node
plot3(FreqRange , ones(size(FreqRange)) , db(total)-min_disto,'b+-')

end
