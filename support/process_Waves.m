function waves = process_Waves(waves)
% this function detects the waves of the plus and minus ports in the input
% structure and converts them into differential and common-mode waves
%

T = 1/sqrt(2)*[1 0 -1 0;0 1 0 -1; 1 0 1 0;0 1 0 1];

% all fields under consideration are MxPxF matrices

fields = fieldnames(waves);
for ii=1:length(fields)
    name = fields{ii};
    % look for W_XXX_pXp
    t = regexp(name,'W_(?<name>[a-zA-Z][a-zA-Z0-9]+)_p(?<number>[1-9][0-9]*)p','names');
    if ~isempty(t)
        % it's a wave at a differential port, look for the other ones and transform
        [M,P,F] = size(waves.(fields{ii}));
        % get the old waves out of there
        Aj = waves.(sprintf('W_%s_p%sp_A',t.name,t.number));
        Bj = waves.(sprintf('W_%s_p%sp_B',t.name,t.number));
        Ak = waves.(sprintf('W_%s_p%sm_A',t.name,t.number));
        Bk = waves.(sprintf('W_%s_p%sm_B',t.name,t.number));
        % and preallocate the new waves
        dA = zeros(M,P,F);
        dB = zeros(M,P,F);
        cA = zeros(M,P,F);
        cB = zeros(M,P,F);
        for mm=1:M
            for pp=1:P
                W = zeros(4,F);
                for ff=1:F
                     W(:,ff) = T*[Aj(mm,pp,ff);Bj(mm,pp,ff);Ak(mm,pp,ff);Bk(mm,pp,ff)];
                end
                dA(mm,pp,:)=W(1,:);
                dB(mm,pp,:)=W(2,:);
                cA(mm,pp,:)=W(3,:);
                cB(mm,pp,:)=W(4,:);
            end
        end
        % put the result in the struct
        waves.(sprintf('W_%s_p%sd_A',t.name,t.number)) = dA;
        waves.(sprintf('W_%s_p%sd_B',t.name,t.number)) = dB;
        waves.(sprintf('W_%s_p%sc_A',t.name,t.number)) = cA;
        waves.(sprintf('W_%s_p%sc_B',t.name,t.number)) = cB;
        
    end
end

% delete the obsolete fields in the struct.
% waves = rmfieldregexp(waves,'^W_[a-zA-Z][a-zA-Z0-9]+_p[1-9][0-9]*[pm]_[AB]$');



end