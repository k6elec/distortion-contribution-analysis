function porttype = savePortType(TotalPorts,diffPorts,unbal2bal)
    porttype={};
    for ii=1:TotalPorts-length(diffPorts)
        if any(ii==diffPorts)
            if unbal2bal
                % the port is a differential port
                porttype{end+1} = [num2str(ii) 'd'];
                porttype{end+1} = [num2str(ii) 'c'];
            else
                % the port is a differential port
                porttype{end+1} = [num2str(ii) 'p'];
                porttype{end+1} = [num2str(ii) 'm'];
            end
        else
            % the port is a single-ended port
            porttype{end+1} = num2str(ii);
        end
    end
end