function [Sparam,CvecG] = unbalanced_to_balanced(Sparam,diffPorts,Z0,CvecG)
% this function converts normal S parameters into mixed mode S-parameters
%
%   Sparam = unbalanced_to_balanced(Sparam,diffPorts,Z0)
%   Sparam = unbalanced_to_balanced(Sparam,diffPorts,Z0,CvecG)
%
% Sparam     is the PxPxF matrix that contains the S-parameters
% diffPorts  is a vector that contains the port numbers of the differential ports
% Z0         is the characteristic impedance used in the conversion
% CvecG      is the covariance matrix of vec(S)
%
% Adam Cooman, ELEC VUB


% 1. find the mixed ports that should be combined
mixedPorts = zeros(length(diffPorts),2);
for ii=1:length(diffPorts)
    currport = diffPorts(ii);
    diffports=sum(diffPorts<currport);
    portnr = 2*diffports+(currport-diffports);
    mixedPorts(ii,:) = [ portnr ; portnr+1 ];
end
%  2. convert the stuff using unbal2bal
if ~isempty(mixedPorts)
    if exist('CvecG','var')
        [Sparam,CvecG] = unbal2bal(Sparam,mixedPorts,Z0,[],CvecG);
    else
        Sparam = unbal2bal(Sparam,mixedPorts,Z0);
    end
end
%  3. permute back to the 'original' port ordering
order = zeros(1,size(Sparam,1));
for ii=1:length(diffPorts)
    currport = diffPorts(ii);
    diffports = sum(diffPorts<currport);
    portnr = 2*diffports+(currport-diffports);
    order(portnr:portnr+1) = [ii length(diffPorts)+ii];
end
order(order==0)=2*length(diffPorts)+1:size(Sparam,1);
if exist('CvecG','var')
    [Sparam,CvecG] = changeOrder(Sparam,order,CvecG);
else
    Sparam = changeOrder(Sparam,order);
    CvecG=[];
end

end


